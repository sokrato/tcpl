#include <stdio.h>
#include <stdlib.h>

/**
 * rtrim input lines
 */
 
 /**
  * rtrim a string 
  * @return int final string length, excluding \0 
  */
int rtrim(char line[], int len)
{
    if( len<2 )
        return 0;
    while( --len>=0 )
        if( ' '==line[len] || '\t'==line[len] || '\n'==line[len] )
            line[len] = '\0';
        else if( '\0'==line[len] )
            continue;
        else
            break;
    return len+1;
}
int reverse(char line[], int len)
{
    int i = len/2;
    char t;
    while( --i>=0 )
    {
        t = line[i];
        line[i] = line[ len-i-1 ];
        line[len-i-1] = t;
    }
    return 1;
}
int getlinex(char line[], int maxlen)
{
    int i, c;
    for(i=0;i<maxlen-1 && (c=getchar())!=EOF && c!='\n'; ++i)
        line[i] = c;
    if( c=='\n' )
        line[i++]=c;
    line[i] = '\0';
    return i;
}
int main(int argc, char* argv[])
{
    char line[100], len;
    int (*sf)(char[], int);
    if( argc>1 && *argv[1]=='r' )
        sf = &reverse;
    else
        sf = &rtrim;
    //rtrim(line, getlinex(line, 100) );
    //printf("One:%s\n", line);
    while( (len=getlinex(line, 100))>0 )
    {
        // printf("recved:0%s0\n", line);
        if( (*sf)(line, len)>0 ){
            printf("%s", line );
            if( &rtrim==sf )
                printf("\n");
        }
    }
    return 0;
}

