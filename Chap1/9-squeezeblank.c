#include <stdio.h>

#define isblank(c) (' '==c || '\n'==c || '\t'==c )

/**
 * compress one or more blanks to one
 */
int main()
{
    int c, n=0;
    while( (c=getchar())!=EOF )
    {
        if( isblank(c) )
            ++n;
        else{
            if( n ){
                putchar(' ');
                n=0;
            }
            putchar(c);
        }
    }
    return 0;
}

