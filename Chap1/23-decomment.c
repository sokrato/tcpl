#include <stdio.h>
#include <stdlib.h>

/**
 * remove all comments from fin, and output code to *fout*
 * // // " '' "' '
 */
int decomment(FILE* fin, FILE* fout)
{
    int c=0/* input char */;
    while( (c=fgetc(fin))!=EOF )
    {
        if( '\''==c ) //single quote read!*
        {
            fputc(c, fout);
            c=fgetc(fin);
            if( '\''==c )
                fputc(c, fout);
            else if( '\\'==c )
            {
                fputc(c, fout);
                c = fgetc(fin);
                fputc(c,fout);
                c = fgetc(fin);
                fputc(c,fout);
            }
            else
            {
                fputc(c,fout);
                c = fgetc(fin);
                fputc(c,fout);
            }
        }
        else if( '\"'==c )
        {
            fputc(c, fout);
            while( (c=fgetc(fin))!=EOF )
            {
                fputc(c, fout);
                if( c=='\\' )
                {
                    c = fgetc(fin);
                    fputc(c,fout);
                }
                else if( '\"'==c )
                    break;
            }
            if( c==EOF )
                break;
        }
        else if( '/'==c )
        {
            int c2 = fgetc(fin);
            if( '/'==c2 )
            {
                do{
                    c=fgetc(fin);
                }while( c!=EOF && c!='\n' );
                if( c==EOF )
                    break;
                else
                    fputc(c, fout);
            }
            else if( '*'==c2 )
            {
                do{
                    c=fgetc(fin);
                    if( '*'==c )
                    {
                        c=fgetc(fin);
                        if( '/'==c )
                            break;
                    }
                }while( EOF!=c );
            }
            else
                fprintf(fout, "%c%c", c, c2);
        }
        else
            fputc(c, fout);
    }
    return 0;
}

int main(int argc, char* argv[])
{
    decomment(stdin, stdout);
    return 0;
}
