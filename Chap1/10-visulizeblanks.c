#include <stdio.h>

/**
 * replace \t with \\t, space with \b, \ with \\
 */
int main(int argc, char* argv[])
{
    int c;
    while( (c=getchar())!=EOF )
    {
        if( ' '== c )
            printf("\\b");
        else if( '\t'==c )
            printf("\\t");
        else if( '\\'==c )
            printf("\\\\");
        else
            putchar(c);
    }
    return 0;
}

