#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    int age;
    char name[100];
    printf("Enter your age and name:\n");
    scanf("%i %s", &age, name);
    printf("Hello %s, %d years old!\n", name, age);
    return 0;
}
