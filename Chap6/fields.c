#include <stdio.h>
#include <stdlib.h>

typedef struct {
unsigned int is_hidden : 1;
unsigned int status    : 2;
unsigned int is_draft  : 1;
} Flag;

int main(int argc, char* argv[])
{
    Flag f;
    f.is_hidden = 1;
    f.status = 3;
    f.is_draft = 6;
    printf("%d,%d,%d\n", f.is_hidden, f.status, f.is_draft);
    return 0;
}
